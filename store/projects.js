export const state = () => ({
  list: [],
  filteredList: [],
  filter: {
    technologies: []
  }
})

export const mutations = {
  set (state, projects) {
    state.list = projects
    state.filteredList = projects
  },
  filter (state, filter) {
    state.filter = filter
    state.filteredList = state.list.filter((p) => {
      for (const tech of filter.technologies) {
        if (!p.technologies.includes(tech)) {
          return false
        }
      }
      return true
    })
  }
}
